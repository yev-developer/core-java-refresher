package io.github.yy.core.datastructures.tree;

import org.junit.Test;

/*
This is a representation of the balanced tree
You can traverse in an in-order, pre-order (root, then left and right) and post-order
 */
public class Node {

    Node left,right;

    int data;

    public Node(int data) {
        this.data = data;
    }


    public void insert(int value) {

        if (value <= data) {
            if (left == null)
                left = new Node(value);
            else
                left.insert(data);
        } else {
            if(right == null)
                right = new Node(data);
            else
                right.insert(data);
        }

    }

    public boolean contains(int value) {

        if(data == value)
            return true;
        else if (value < data) {

            if (left == null) {
                return false;
            } else {
                return left.contains(data);
            }

        } else {
            if (right == null) {
                return false;

            } else {

                return right.contains(data);
            }

        }
    }

    public void printInOrder() {

        if (left != null) {
            left.printInOrder();
        }
        System.out.println(data);

        if (right != null) {
            right.printInOrder();
        }


    }






  




}
