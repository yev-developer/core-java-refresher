package io.github.yy.core.datastructures.graph;

import java.util.LinkedList;

/*
DFS => recursive
BFS => iterative, using queue. Need to flag to prevent infinite loop. isVisited
https://www.youtube.com/watch?v=zaBhtODEL0w&t=58s
 */
public class Graph {

    public static class Node {
        private int id;

        LinkedList<Node> adjacent = new LinkedList<>();

        public Node(int id) {
            this.id = id;
        }

    }

    private Node getNode(int id) {

        return null;
    }

}
