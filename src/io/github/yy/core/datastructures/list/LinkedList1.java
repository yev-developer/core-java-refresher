package io.github.yy.core.datastructures.list;

/**
 *
 * @param <T>
 */
public class LinkedList1<T> {
	private T element;
	private LinkedList1<T> next;

	public LinkedList1(T element, LinkedList1<T> next) {
		this.element = element;
		this.next = next;
	}

	public T getElement() {
		return element;
	}

	public LinkedList1<T> getNext() {
		return next;
	}
	
	public static <T> LinkedList1<T> reverse(final LinkedList1<T> original) {
	    if (original == null) {
	        throw new NullPointerException("Cannot reverse a null list");
	    }

	    if(original.getNext() == null) {
	        return original;
	    }
	    final LinkedList1<T> next = original.next;
	    original.next = null;

	    final LinkedList1<T> othersReversed = reverse(next);

	    next.next = original;

	    return othersReversed;
	}
}
