package io.github.yy.core.concurrency.racecondition;

public class UseCounter implements Runnable {

	//For a static method synchronized is acquired on the class UseCounter.class. Every class is associate with an object of Class type
	public static synchronized void incrementStatic() {
		Counter.count++;
		System.out.print(Counter.count + "  ");
		
		//Same as
		synchronized (UseCounter.class) {
			
		}
	}
	
	//Synchronized constructor => compilation error
	
	public void increment() {
		// increments the counter and prints the value
		// of the counter shared between threads
		
		synchronized (this) {
			Counter.count++;
			System.out.print(Counter.count + "  ");
			
		}
	}

	@Override
	public void run() {
		
		increment();
        increment();
        increment();

	}

}
