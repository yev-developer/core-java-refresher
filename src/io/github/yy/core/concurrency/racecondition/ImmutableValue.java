package io.github.yy.core.concurrency.racecondition;

/**
 * The following object is immutable, so no race condition can occur There no
 * setters or getters we will create a new object and re-assign values of the
 * previous The references to this object will not be immutable
 * 
 */
public class ImmutableValue {

	private final Integer value;

	public ImmutableValue(Integer value) {
		this.value = value;
	}

	public int getValue() {
		return this.value;
	}

	public ImmutableValue add(int valueToAdd) {
		return new ImmutableValue(this.value + valueToAdd);
	}

}

// It is not thread-safe. we can define get/set add as synchronized to protect
class Calculator {
	private ImmutableValue currentValue = null;

	public ImmutableValue getValue() {
		return currentValue;
	}

	public void setValue(ImmutableValue newValue) {
		this.currentValue = newValue;
	}

	public void add(int newValue) {
		this.currentValue = this.currentValue.add(newValue);
	}
}
