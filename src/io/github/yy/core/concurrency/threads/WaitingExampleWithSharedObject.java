package io.github.yy.core.concurrency.threads;

/*
 * ANy calls to wait() or notify() should be within a synchronized block
 * The reason for this (other than the standard thread safety concerns) is due to something known as a missed signal.
 */
public class WaitingExampleWithSharedObject {

	public static void main(String[] args) {

		WaitingExampleWithSharedObject exmpale = new WaitingExampleWithSharedObject();
		
		Decision decision = exmpale.new Decision("Decision to fly");
		
		Thread plane1 = new Thread(exmpale.new Airoplane(decision));
		Thread plane2 = new Thread(exmpale.new Airoplane(decision));
		
		Thread controlCentre = new Thread(exmpale.new FlightControl(decision));
		
		System.out.println("Starting a process");
		
		
		plane1.start();
		plane2.start();
		
		controlCentre.start();
		
		synchronized (plane1) {
			
		}
		System.out.println("Finished with the process");
	}
	
	class Decision{
		private final String name;

		public Decision(String name) {
			super();
			this.name = name;
		}

		public String getName() {
			return name;
		}
		
		
		
	}

	class FlightControl implements Runnable {

		private final Decision decision;

		public FlightControl(Decision decision) {
			super();
			this.decision = decision;
		}

		@Override
		public void run() {
			String name = Thread.currentThread().getName();
			
			for(int i = 1; i <= 5; i++){
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				System.out.println("Check " + i + " is done ");
			}
			
			System.out.println("Notifying all waiting planes");
			
			synchronized (decision) {
//				decision.notify();
				decision.notifyAll();
			}
			System.out.println("Bye!");
		}

	}

	class Airoplane implements Runnable {

		private final Decision decision;

		public Airoplane(Decision decision) {
			super();
			this.decision = decision;
		}

		@Override
		public void run() {
			String name = Thread.currentThread().getName();
			System.out.println(name + " is waiting for your command");

//			obj.wait() can only be called if the current thread holds the lock
			try {
				synchronized (decision) {
					decision.wait();
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			System.out.println(name + " is taking off");

		}

	}

}
