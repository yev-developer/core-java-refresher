package io.github.yy.core.concurrency.futures;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

public class CompletableFuturesExample {

    public static void main(String... arg) {

        ExecutorService pool = Executors.newFixedThreadPool(10);

        CompletableFuture<String> future1
                = CompletableFuture.supplyAsync(() -> {
                    System.out.println("First");

                    System.out.println(Thread.currentThread().getName());

                    try {
                        Thread.sleep(5000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    return "First";
                }, pool);
        CompletableFuture<String> future2
                = CompletableFuture.supplyAsync(() -> {

                    System.out.println("Second");

                    System.out.println(Thread.currentThread().getName());
                    try {
                        Thread.sleep(5000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    return "Second";
                }, pool);
        CompletableFuture<String> future3
                = CompletableFuture.supplyAsync(() -> {

                    System.out.println("Third");

                    System.out.println(Thread.currentThread().getName());

                    try {
                        Thread.sleep(5000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    return "Third";

                }, pool);


        CompletableFuture<List<String>> results =
                CompletableFuturesExample.sequence(Arrays.asList(future1, future2, future3));

        try {
            results.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        System.out.println("Finished");

        pool.shutdown();

//        CompletableFuture<Void> combinedFuture
//                = CompletableFuture.allOf(future1, future2, future3);
//
//        try {
//            combinedFuture.get();
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        } catch (ExecutionException e) {
//            e.printStackTrace();
//        }
    }

    public static CompletableFuture<List<String>> sequence(List<CompletableFuture<String>> futures ){

        CompletableFuture<Void> allDone = CompletableFuture.allOf(futures.toArray(new CompletableFuture[0]));

        return allDone.thenApply(v ->
                futures.stream().map(CompletableFuture::join).collect(Collectors.toList())
        );

    }


}
