package io.github.yy.core.concurrency.datastructure;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class CopyOnArrayListTest {

	public static void main(String[] args) {
		
//		List<String> list = new ArrayList<>();//ArrayList is a fail-fast if modification is happening outside of Iterator inner mechanism
		List<String> list = new CopyOnWriteArrayList<>();
		list.add("A");
		list.add("B");
		list.add("C");
		
		Iterator<String> iter = list.iterator();
		
		while(iter.hasNext()) {
			System.out.println(iter.next());
			list.remove(0);//ConcurrentModificationException when ArrayList
//			iter.remove();//UnsupportedOperationException when Copy...
			list.add("F");//ConcurrentModificationException when ArrayList
		}
		
		System.out.println(list);

	}

}
