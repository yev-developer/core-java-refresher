package io.github.yy.core.algorithms.strings;

import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class MergeCharacters {

    public String mergeCharactersIntoSeequenceOfStrings(String args) {

        char[] characters = args.toCharArray();

        StringBuilder sb = new StringBuilder();

        if (characters.length == 0 ) {
            return "";
        } else if (characters.length == 1) {
            return args;
        }

        char previousCharacter = characters[0];
        int previousCounter = 0;

        int i = 0;

        while (i < characters.length) {

            if (i == 0) {
                previousCounter++ ;
                i++;
                continue;
            }

            System.out.println(characters[i]);
            if (characters[i] == previousCharacter) {
                previousCounter++;
            }
            else{

                sb.append(previousCharacter);
                sb.append(previousCounter);

                previousCharacter = characters[i];
                previousCounter = 0;

            }



            i++;
        }


        return sb.toString();

    }

    @Test
    public void testIfCharactersAreMerged() {

        MergeCharacters mc = new MergeCharacters();

        String s1 = "aaabba";
        assertEquals("a3b2a", mc.mergeCharactersIntoSeequenceOfStrings(s1));

    }
}
